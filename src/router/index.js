import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    icon: 'mdi-home',
    name: 'home',
    component: Home
  },
  // {
  //   path: '/about',
  //   name: 'about',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  {
    path: '/dashboard',
    icon: 'mdi-view-dashboard',
    name: 'Dashboard',
    component: () => import('@/views/Dashboard')
  },
  {
    path: '/setting',
    icon: 'mdi-settings',
    name: 'Settings',
    component: () => import('@/views/Settings')
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

export default router
